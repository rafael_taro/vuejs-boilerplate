// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import VueRouter from 'vue-router';
import { routes } from './router/routes';

import './styles/main.scss'

Vue.use(VueRouter);

const router = new VueRouter({
  routes: routes,
//  mode: 'history'
});

router.push('/sign-in')

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router: router,
  components: { App },
  template: '<App/>'
})
