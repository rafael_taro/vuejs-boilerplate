import SignIn from '../components/SignIn.vue';
import Error404 from '../components/Error404.vue';
import Dashboard from '../components/Dashboard.vue';
import Quiz from '../components/Quiz.vue';
import Relatorio from '../components/Relatorio.vue';


export const routes = [{
    path: '/sign-in',
    name: 'signIn',
    component: SignIn
  },
  {
    path: '/dashboard',
    name: 'dashboard',
    component: Dashboard,
    meta: {
      requiresAuth: false
    }
  },
  {
    path: '/quiz',
    name: 'quiz',
    component: Quiz,
    meta: {
      requiresAuth: false
    }
  },
  {
    path: '/relatorio',
    name: 'relatorio',
    component: Relatorio,
    meta: {
      requiresAuth: false
    }
  },
  {
    path: '/404',
    name: '404',
    component: Error404
  }
]