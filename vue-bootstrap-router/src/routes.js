import Vue from 'vue';
import Router from 'vue-router';
import Login from '@/components/Login';

export const routes = [
    {
      path: '/',
      name: 'Login',
      component: Login
    }
  ]
